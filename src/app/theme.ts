import { extendTheme } from '@chakra-ui/react';

const theme = extendTheme({
  config: {
    initialColorMode: 'light',
    useSystemColorMode: true,
  },
  components: {
    Button: {
      baseStyle: {
        _focus: { boxShadow: 'none' },
      },
    },
  },
});

export default theme;
