import React from 'react';

import { ChakraProvider, ColorModeScript } from '@chakra-ui/react';
import theme from './theme';
import Header from './components/Header';
import Background from './components/Background';

const App = () => {
  return (
    <ChakraProvider theme={theme}>
      <ColorModeScript initialColorMode={theme.config.initialColorMode} />
      <Header />
      <Background />
    </ChakraProvider>
  );
};

export default App;
