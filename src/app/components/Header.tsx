import { Flex } from '@chakra-ui/react';
import React from 'react';
import ThemeToggler from './ThemeToggler';

const Header: React.FC = () => {
  return (
    <Flex>
      <ThemeToggler />
    </Flex>
  );
};

export default Header;
