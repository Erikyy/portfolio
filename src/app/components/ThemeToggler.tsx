import React from 'react';
import { Button, useColorMode } from '@chakra-ui/react';
import { FaSun, FaMoon } from 'react-icons/fa';

const ThemeToggler: React.FC = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  return <Button onClick={toggleColorMode}>{colorMode === 'dark' ? <FaSun /> : <FaMoon />}</Button>;
};
export default ThemeToggler;
