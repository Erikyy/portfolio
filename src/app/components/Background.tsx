import React from 'react';
import { Canvas } from '@react-three/fiber';
import { RoundedBox, PerspectiveCamera } from '@react-three/drei';
import { Box } from '@chakra-ui/react';

const Background: React.FC = () => {
  return (
    <Box position="fixed" width="100%" height="100%">
      <Canvas>
        <ambientLight />
        <PerspectiveCamera makeDefault />
        <RoundedBox args={[1, 1, 1]} position={[1, 0, -5]} radius={0.05} smoothness={4}>
          <meshPhongMaterial attach="material" color="#f3f3f3" wireframe />
        </RoundedBox>
      </Canvas>
    </Box>
  );
};

export default Background;
